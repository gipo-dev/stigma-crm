<?php

return [

    'order' => [
        'payment_type' => [
            'card' => 'Предоплата',
            'cash' => 'Наличные',
        ],
        'statuses' => [
            'keys' => [
                \App\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT => 'Оставлена заявка',
                \App\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM => 'Подтверждение заявки менеджером',
                \App\Models\OrderEvent::EVENT_WAITING_FOR_PAYMENT => 'Получение оплаты',
                \App\Models\OrderEvent::EVENT_ASSEMBLY => 'Сборка заказа на складе',
                \App\Models\OrderEvent::EVENT_DELIVERY_TO_TRANSPORT_COMPANY => 'Доставка в транспортную компанию',
                \App\Models\OrderEvent::EVENT_DELIVERY => 'Доставка',
                \App\Models\OrderEvent::EVENT_FINISH => 'Заказ получен клиентом',
                \App\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT => 'Вручение заказа клиенту',
            ],
            'titles' => [
                \App\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT => 'Оставлена заявка',
                \App\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM => 'Подтверждение заявки менеджером',
                \App\Models\OrderEvent::EVENT_WAITING_FOR_PAYMENT => 'Подтверждение оплаты',
                \App\Models\OrderEvent::EVENT_ASSEMBLY => 'Сборка заказа на складе',
                \App\Models\OrderEvent::EVENT_DELIVERY_TO_TRANSPORT_COMPANY => 'Доставка в транспортную компанию',
                \App\Models\OrderEvent::EVENT_DELIVERY => 'Доставка',
                \App\Models\OrderEvent::EVENT_FINISH => 'Получение заказа клиентом',
                \App\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT => 'Вручение заказа клиенту',
                '' => 'Заказ получен клиентом',
            ],
            'colors' => [
                \App\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT => \App\Models\OrderEvent::COLOR_SUCCESS,
                \App\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM => \App\Models\OrderEvent::COLOR_WARNING,
                \App\Models\OrderEvent::EVENT_WAITING_FOR_PAYMENT => \App\Models\OrderEvent::COLOR_DANGER,
                \App\Models\OrderEvent::EVENT_ASSEMBLY => \App\Models\OrderEvent::COLOR_PRIMARY,
                \App\Models\OrderEvent::EVENT_DELIVERY_TO_TRANSPORT_COMPANY => \App\Models\OrderEvent::COLOR_PRIMARY,
                \App\Models\OrderEvent::EVENT_DELIVERY => \App\Models\OrderEvent::COLOR_PRIMARY,
                \App\Models\OrderEvent::EVENT_FINISH => \App\Models\OrderEvent::COLOR_WARNING,
                \App\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT => \App\Models\OrderEvent::COLOR_WARNING,
                '' => \App\Models\OrderEvent::COLOR_SUCCESS,
            ],
            'types' => [
                'card' => [
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT,
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM,
                    \App\Models\OrderEvent::EVENT_WAITING_FOR_PAYMENT,
                    \App\Models\OrderEvent::EVENT_ASSEMBLY,
                    \App\Models\OrderEvent::EVENT_DELIVERY_TO_TRANSPORT_COMPANY,
                    \App\Models\OrderEvent::EVENT_DELIVERY,
                    \App\Models\OrderEvent::EVENT_FINISH,
                ],
                'cash' => [
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT,
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM,
                    \App\Models\OrderEvent::EVENT_ASSEMBLY,
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT,
                    \App\Models\OrderEvent::EVENT_FINISH,
                ],
            ],
            'buttons' => [
                'card' => [
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT => 'Подтвердить заявку',
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM => 'Подтвердить оплату',
                    \App\Models\OrderEvent::EVENT_WAITING_FOR_PAYMENT => 'Заказ собран',
                    \App\Models\OrderEvent::EVENT_ASSEMBLY => 'Заказ доставляется в тк',
                    \App\Models\OrderEvent::EVENT_DELIVERY_TO_TRANSPORT_COMPANY => 'Заказ отправлен через тк',
                    \App\Models\OrderEvent::EVENT_DELIVERY => 'Заказ получен клиентом',
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT => 'Клиент забрал и оплатил заказ',
                ],
                'cash' => [
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT => 'Подтвердить заявку',
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM => 'Заказ собран',
                    \App\Models\OrderEvent::EVENT_ASSEMBLY => 'Заказ готов к вручению',
                    \App\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT => 'Клиент оплатил заказ и получил его',
                    \App\Models\OrderEvent::EVENT_FINISH => 'Клиент оплатил заказ и получил его',
                ],
            ],
        ],
    ],

];
