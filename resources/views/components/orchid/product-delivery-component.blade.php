<div>
    <div style="padding: 15px 20px;background:#eee;border: 2px solid #ddd;border-radius: 6px;margin-bottom: 15px;">
        <h4>Адрес ПВЗ</h4>
        <p style="margin-bottom: 0;">
            {{ $value->delivery->type }}, {{ $value->delivery->adress }}
        </p>
    </div>
</div>
