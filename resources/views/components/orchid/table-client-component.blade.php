<div>
    <p style="font-size: 16px;margin-bottom: 0;">
        {{ $order->client->short_name ?? '' }}
    </p>
    <div style="color: #999">
        {{ $order->client->email ?? '' }} / {{ $order->client->id ?? '' }}
    </div>
</div>
