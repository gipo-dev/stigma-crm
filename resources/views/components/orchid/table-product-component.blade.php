<div>
    <a href="{{ route('platform.products.view', $product['id']) }}" style="display:flex;">
        <img src="{{ $product['photo'] }}" style="height: 42px;">
        <div style="margin-left: 10px;">
            <p style="font-size: 16px;margin-bottom: 0;">
                {{ $product['name'] ?? '' }}
            </p>
            <div style="color: #999">
                id {{ $product['id'] ?? '' }}
            </div>
        </div>
    </a>
</div>
