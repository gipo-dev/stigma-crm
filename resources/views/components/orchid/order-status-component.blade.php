<div style="margin-bottom: 20px;">
    <div class="col p-0 px-3">
        <legend class="text-black">
            История заказа
        </legend>
    </div>
    <div class="bg-white rounded shadow-sm p-4 py-4 d-flex flex-column">

        <div class="d-flex">
            @foreach($statuses as $i => $key)
                <div class="d-flex order-status
                    @if($key < $nextStatusId)
                        active
                    @elseif($key == $nextStatusId)
                        current
                    @endif">
                    <div>
                        <span class="order-status__date">
                        @isset($events[$key])
                            {{ $events[$key]->created_at->format('d.m.Y H:i') }}
                        @else
                            &zwnj;
                        @endisset
                        </span>
                        {{ __('admin.order.statuses.keys.' . $key) }}
                    </div>
                    @if($i < count($statuses) - 1)
                        <div style="white-space: nowrap;margin-left: 10px;margin-right: 10px;">
                            <span class="order-status__date">&zwnj;</span>
                            <svg width="25" height="8" viewBox="0 0 25 8" style="fill:currentColor;"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M24.3536 4.35355C24.5488 4.15829 24.5488 3.84171 24.3536 3.64645L21.1716 0.464466C20.9763 0.269204 20.6597 0.269204 20.4645 0.464466C20.2692 0.659728 20.2692 0.976311 20.4645 1.17157L23.2929 4L20.4645 6.82843C20.2692 7.02369 20.2692 7.34027 20.4645 7.53553C20.6597 7.7308 20.9763 7.7308 21.1716 7.53553L24.3536 4.35355ZM0 4.5H24V3.5H0V4.5Z"/>
                            </svg>
                        </div>
                    @endif
                </div>
            @endforeach
        </div>

    </div>

    <style>
        .order-status {
            color: #A2A4AE;
            justify-content: flex-start;
        }

        .order-status.current {
            font-weight: 500;
            color: #2DB47B;
        }

        .order-status.active {
            color: #222;
        }

        .order-status__date {
            display: block;
            color: #A2A4AE;
            font-weight: 400;
            font-size: .8em;
        }
    </style>
</div>
