<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEventIdFieldToOrderEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_events', function (Blueprint $table) {
            $table->after('order_id', function (Blueprint $table) {
                $table->smallInteger('event_id')->unsigned()->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_events', function (Blueprint $table) {
            //
        });
    }
}
