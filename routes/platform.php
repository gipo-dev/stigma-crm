<?php

declare(strict_types=1);

use App\Orchid\Screens\Category\CategoryEditScreen;
use App\Orchid\Screens\Category\CategoryListScreen;
use App\Orchid\Screens\Client\ClientEditScreen;
use App\Orchid\Screens\Client\ClientListScreen;
use App\Orchid\Screens\Order\OrderEditScreen;
use App\Orchid\Screens\Order\OrderListScreen;
use App\Orchid\Screens\ParentProduct\ParentProductEditScreen;
use App\Orchid\Screens\ParentProduct\ParentProductListScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Product\ProductEditScreen;
use App\Orchid\Screens\Product\ProductListScreen;
use App\Orchid\Screens\Product\ProductViewScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->push('Главная', route('platform.main'));
    });

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{users}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Edit'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
//            ->parent('platform.systems.index')
            ->push(__('Users'), route('platform.systems.users'));
    });

// Platform > System > Roles > Role
Route::screen('roles/{roles}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
//            ->parent('platform.systems.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });

// Platform > Products > View
Route::screen('products/{person}/view', ProductViewScreen::class)
    ->name('platform.products.view')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.products')
            ->push('Редактирование');
    });

// Platform > Products > Edit
Route::screen('products/{person}/edit', ProductEditScreen::class)
    ->name('platform.products.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.products')
            ->push('Редактирование');
    });

// Platform > Products > Add
Route::screen('products/create', ProductEditScreen::class)
    ->name('platform.products.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.products')
            ->push('Добавить');
    });

// Platform > Products
Route::screen('products', ProductListScreen::class)
    ->name('platform.products')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.main')
            ->push('Ассортимент', route('platform.products'));
    });

// Platform > Parent Products > Edit
Route::screen('parent_products/{parent_product}/edit', ParentProductEditScreen::class)
    ->name('platform.parent_products.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.parent_products')
            ->push('Редактирование');
    });

// Platform > Parent Products > Add
Route::screen('parent_products/create', ParentProductEditScreen::class)
    ->name('platform.parent_products.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.parent_products')
            ->push('Добавить');
    });

// Platform > Parent Products
Route::screen('parent_products', ParentProductListScreen::class)
    ->name('platform.parent_products')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.main')
            ->push('Ассортимент', route('platform.parent_products'));
    });

// Platform > Category > Edit
Route::screen('categories/{category}/edit', CategoryEditScreen::class)
    ->name('platform.categories.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.categories')
            ->push('Редактирование');
    });

// Platform > Category > Add
Route::screen('categories/create', CategoryEditScreen::class)
    ->name('platform.categories.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.categories')
            ->push('Добавить');
    });

// Platform > Categories
Route::screen('categories', CategoryListScreen::class)
    ->name('platform.categories')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.main')
            ->push('Ассортимент', route('platform.categories'));
    });

// Platform > Clients > Edit
Route::screen('clients/{client}/edit', ClientEditScreen::class)
    ->name('platform.clients.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.clients')
            ->push('Редактирование');
    });

// Platform > Clients > Add
Route::screen('clients/create', ClientEditScreen::class)
    ->name('platform.clients.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.clients')
            ->push('Добавить');
    });

// Platform > Clients
Route::screen('clients', ClientListScreen::class)
    ->name('platform.clients')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.main')
            ->push('Клиенты', route('platform.clients'));
    });

// Platform > Orders > Edit
Route::screen('orders/{order}/edit', OrderEditScreen::class)
    ->name('platform.orders.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.orders')
            ->push('Редактирование');
    });

// Platform > Orders
Route::screen('orders', OrderListScreen::class)
    ->name('platform.orders')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.main')
            ->push('Клиенты', route('platform.orders'));
    });
