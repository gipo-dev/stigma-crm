<?php

namespace App\View\Components\Orchid\OrderStatusButtons;

use Illuminate\View\Component;

class WaitForPaymentComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.orchid.order-status-buttons.wait-for-payment-component');
    }
}
