<?php

namespace App\View\Components\Orchid;

use App\Models\Order;
use Illuminate\View\Component;

class TableClientComponent extends Component
{
    /**
     * @var \App\Models\Order
     */
    private $order;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.orchid.table-client-component', ['order' => $this->order]);
    }
}
