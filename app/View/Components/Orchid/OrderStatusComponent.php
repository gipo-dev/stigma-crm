<?php

namespace App\View\Components\Orchid;

use App\Models\Order;
use Illuminate\View\Component;

class OrderStatusComponent extends Component
{
    /**
     * @var \App\Models\Order
     */
    private $order;

    /**
     * @var int
     */
    private $status;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.orchid.order-status-component', [
            'order' => $this->order,
            'currentStatusId' => $this->order->eventService()->getStatus(),
            'events' => $this->order->events->keyBy('event_id'),
            'statuses' => __('admin.order.statuses.types.' . $this->order->payment_type),
            'nextStatusId' => $this->order->eventService()->nextStatus(),
//            'status' => $this->status,
        ]);
    }
}
