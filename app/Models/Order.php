<?php

namespace App\Models;

use App\Services\OrderEventService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Order extends Model
{
    use AsSource;
    use Filterable;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $with = [
        'client',
        'events',
        'delivery',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'cart' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function delivery()
    {
        return $this->belongsTo(OrderDelivery::class, 'delivery_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class, 'chat_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany(OrderEvent::class)->latest();
    }

    /**
     * @return \App\Services\OrderEventService
     */
    public function eventService()
    {
        return new OrderEventService($this);
    }

    /**
     * @return float|int
     */
    public function getTotalSumAttribute()
    {
        $total = 0;
        foreach ($this->cart['cart'] ?? [] as $item) {
            $total += ($item['price'] ?? 1) * ($item['quantity'] ?? 1);
        }
        return $total;
    }
}
