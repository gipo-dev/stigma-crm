<?php

namespace App\Services;

use App\Models\Product;

class ProductService
{
    public function getProductPrice(Product $product, $count)
    {
        $currentPrice = 0;
        if (!$product->prices)
            return $product['price'];
        foreach ($product->prices as $price) {
            if ($price['to'] == null || $count < $price['to'])
                $currentPrice = $price['price'];
        }
        return $currentPrice;
    }
}
