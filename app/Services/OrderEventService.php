<?php

namespace App\Services;

use App\Models\Order;
use App\Models\OrderEvent;

class OrderEventService
{
    /**
     * @var \App\Models\Order
     */
    private $order;

    /**
     * @var int
     */
    private $_status;

    /**
     * @var int
     */
    private $_nextStatus;

    /**
     * @param \App\Models\Order $order
     */
    public function __construct(Order $order)
    {
        if ($order->events->count() < 1) {
            $order->events()->create(['event_id' => OrderEvent::EVENT_WAIT_FOR_ACCEPT,]);
            $order = $order->refresh();
        }
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        if (!$this->_status)
            $this->_status = $this->order->events->sortByDesc('id')->first()->event_id;
        return $this->_status;
    }

    /**
     * @return int|null
     */
    public function nextStatus()
    {
        if (!$this->_nextStatus) {
            $statuses = __('admin.order.statuses.types.' . $this->order->payment_type);
            $this->_nextStatus = null;
            if (($statusIndex = array_search($this->getStatus(), $statuses)) !== false) {
                $this->_nextStatus = $statuses[$statusIndex + 1] ?? null;
            }
        }
        return $this->_nextStatus;
    }
}
