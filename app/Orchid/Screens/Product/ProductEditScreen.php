<?php

namespace App\Orchid\Screens\Product;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductType;
use App\Orchid\Layouts\EditScreenButtons;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ProductEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование позиции';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\ProductType
     */
    private $product;

    /**
     * Query data.
     *
     * @param \App\Models\ProductType $product
     * @return array
     */
    public function query(ProductType $product): array
    {
        $this->product = $product;

        if (!$this->product->exists)
            $this->name = 'Добавление позиции';

        return [
            'product' => $product,
            'prices' => $product->prices,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons(
            $this->product->id,
//            $this->product->exists ? route('page.products.person', $this->person->slug) : null,
        );
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [

            Layout::columns([
                Layout::rows([
                    Relation::make('product.product_id')->fromModel(Product::class, 'name')
                        ->title('Товар')->required(),
                    Input::make('product.name')->title('Наименование')
                        ->maxlength(255)->required(),
//                    Relation::make('product.category_id')->fromModel(ProductCategory::class, 'name')
//                        ->title('Категория')->required(),
                    Input::make('product.quantity')->type('number')
                        ->title('Количество позиций на складе')->required(),
                    $this->getPricesField(),
                    Input::make('product.manufacturer')->maxlength(255)
                        ->title('Производитель')->required(),
                    TextArea::make('product.description')->rows(10)
                        ->title('Описание')->required(),

                    Input::make('product.id')->hidden(),

                    EditScreenButtons::saveButton('Сохранить позицию'),
                ]),
                Layout::rows([
                    Picture::make('product.photo')->title('Изображение')->required(),
                ]),
            ]),

        ];
    }

    /**
     * @return \Orchid\Screen\Fields\Group
     */
    private function getPricesField()
    {
        $prices = [];
        foreach ([
                     'Цена до 60 шт в заказе',
                     'Цена до 120 шт в заказе',
                     'Цена более 120 шт в заказе',
                 ] as $index => $text) {
            $prices[] = Input::make("prices.$index.price")->type('number')
                ->title($text)->required();
        }
        return Group::make($prices);
    }

    /**
     * @param \App\Models\ProductType $product
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function save(ProductType $product, Request $request)
    {
        $product->fill($request->product)->save();
        $this->savePrices($product, $request);
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param \App\Models\ProductType $product
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    private function savePrices(ProductType $product, Request $request)
    {
        $product->prices()->delete();
        foreach ([[null, 60], [60, 120], [120, null]] as $i => $prices) {
            $product->prices()->create([
                'main' => $i == 0,
                'from' => $prices[0],
                'to' => $prices[1],
                'price' => $request->prices[$i]['price'],
            ]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        ProductType::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.products'));
    }


}
