<?php

namespace App\Orchid\Screens\Product;

use App\Models\ProductCategory;
use App\Models\ProductType;
use App\Orchid\Layouts\EditScreenButtons;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ProductViewScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = '';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\ProductType
     */
    private $product;

    /**
     * Query data.
     *
     * @param \App\Models\ProductType $product
     * @return array
     */
    public function query(ProductType $product): array
    {
        $this->product = $product;

        $this->name = $product->full_name;

        return [
            'array' => [''],
            'product' => $product,
            'prices' => $product->prices,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Редактировать')
                ->icon('pencil')
                ->type(Color::PRIMARY())
                ->href(route('platform.products.edit', $this->product)),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [

            Layout::columns([

                Layout::legend('product', [
                    Sight::make('full_name', 'Наименование'),
                    Sight::make('parent.category.name', 'Категория'),
                    Sight::make('quantity', 'Остаток на складе'),
                    Sight::make('prices', 'Цена ₽/шт')->render(function (ProductType $product) {
                        return Group::make([
                            Input::make('', '')->value(function () {
                                return $this->product->prices[0]->price ?? '';
                            })->readonly()->title('До 60 штук в заказе'),
                            Input::make('', '')->value(function () {
                                return $this->product->prices[1]->price ?? '';
                            })->readonly()->title('До 120 штук в заказе'),
                            Input::make('', '')->value(function () {
                                return $this->product->prices[2]->price ?? '';
                            })->readonly()->title('От 120 штук в заказе'),
                        ])->render()->render();
                    }),
                    Sight::make('description', 'Описание'),
                ]),

                Layout::table('array', [
                    TD::make('')->render(function () {
                        return "<img src='{$this->product->photo}' width='300'>";
                    }),
                ]),

            ]),

        ];
    }

    /**
     * @return \Orchid\Screen\Fields\Group
     */
    private function getPricesField()
    {
        $prices = [];
        foreach ([
                     'Цена до 60 шт в заказе',
                     'Цена до 120 шт в заказе',
                     'Цена более 120 шт в заказе',
                 ] as $index => $text) {
            $prices[] = Input::make("prices.$index.price")->type('number')
                ->title($text)->required();
        }
        return Group::make($prices);
    }

    /**
     * @param \App\Models\ProductType $product
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function save(ProductType $product, Request $request)
    {
        $product->fill($request->product)->save();
        $this->savePrices($product, $request);
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param \App\Models\ProductType $product
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    private function savePrices(ProductType $product, Request $request)
    {
        $product->prices()->delete();
        foreach ([[null, 60], [60, 120], [120, null]] as $i => $prices) {
            $product->prices()->create([
                'main' => $i == 0,
                'from' => $prices[0],
                'to' => $prices[1],
                'price' => $request->prices[$i]['price'],
            ]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        ProductType::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.products'));
    }


}
