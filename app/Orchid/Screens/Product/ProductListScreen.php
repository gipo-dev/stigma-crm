<?php

namespace App\Orchid\Screens\Product;

use App\Models\ProductType;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ProductListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Ассортимент';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'products' => ProductType::filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить товар')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.products.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('products', [

                TD::make('name', 'Имя')
                    ->render(function (ProductType $product) {
                        return Link::make($product->full_name)->route('platform.products.view', $product);
                    }),

//                TD::make('article', 'Артикул'),

                TD::make('', 'Категория')
                    ->render(function (ProductType $product) {
                        return $product->parent->category->name ?? '';
                    }),

                TD::make('', 'Цена')
                    ->render(function (ProductType $product) {
                        return $product->prices->min('price') . ' - ' . $product->prices->max('price') . ' ₽/шт';
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (ProductType $product) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make('Просмотр')
                                    ->route('platform.products.view', $product)
                                    ->type(Color::WARNING())
                                    ->icon('eye'),

                                Link::make(__('Edit'))
                                    ->route('platform.products.edit', $product)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $product->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        ProductType::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
