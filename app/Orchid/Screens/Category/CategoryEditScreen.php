<?php

namespace App\Orchid\Screens\Category;

use App\Models\ProductCategory;
use App\Models\ProductCategoryCategory;
use App\Models\ProductCategoryType;
use App\Orchid\Layouts\EditScreenButtons;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class CategoryEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование категории';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\ProductCategory
     */
    private $category;

    /**
     * Query data.
     *
     * @param \App\Models\ProductCategory $category
     * @return array
     */
    public function query(ProductCategory $category): array
    {
        $this->category = $category;

        if (!$this->category->exists)
            $this->name = 'Добавление категории';

        return [
            'category' => $category,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons(
            $this->category->id,
//            $this->ProductCategory->exists ? route('page.ProductCategorys.person', $this->person->slug) : null,
        );
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [

            Layout::columns([
                Layout::rows([
                    Input::make('category.name')->title('Название')
                        ->maxlength(255)->required(),

                    Input::make('category.id')->hidden(),

                    EditScreenButtons::saveButton('Сохранить категорию'),
                ]),
            ]),

        ];
    }

    /**
     * @param \App\Models\ProductCategory $category
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function save(ProductCategory $category, Request $request)
    {
        $category->fill($request->category)->save();
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        ProductCategory::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.categories'));
    }


}
