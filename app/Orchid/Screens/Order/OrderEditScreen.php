<?php

namespace App\Orchid\Screens\Order;

use App\Models\Order;
use App\Models\OrderEvent;
use App\Models\Product;
use App\Orchid\Layouts\Order\OrdersTable;
use App\Services\ProductService;
use App\View\Components\Orchid\OrderStatusComponent;
use App\View\Components\Orchid\ProductDeliveryComponent;
use App\View\Components\Orchid\TableProductComponent;
use http\Client\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\ViewField;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;

class OrderEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Заказ #';

    /**
     * @var \App\Models\Order
     */
    private $order;

    /**
     * Query data.
     *
     * @param \App\Models\Order $order
     * @return array
     */
    public function query(Order $order): array
    {
        $this->order = $order;

        $this->name .= $this->order->id;

        return [
            'order' => $order,
            'orders' => [$order],
            'products' => $this->order->cart['cart'],
            'messages' => $order->messages,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        $total = 0;
        foreach ($this->order->cart['cart'] as $item) {
            $price = (new ProductService())
                ->getProductPrice((new Product($item)), $item['quantity'] ?? 0);
            $total += $item['quantity'] * $price;
        }

        return [

            OrdersTable::class,

            Layout::table('products', [

                TD::make('', 'Наименование')
                    ->component(TableProductComponent::class, 'product'),

                TD::make('', 'Количество, шт')->render(function ($item) {
                    return $item['quantity'];
                }),

                TD::make('', 'Цена, шт')->render(function ($item) {
                    return (new ProductService())
                        ->getProductPrice((new Product($item)), $item['quantity'] ?? 0);
                }),

                TD::make('', 'Сумма заказа')
                    ->render(function ($item) use (&$total) {
                        $price = (new ProductService())
                            ->getProductPrice((new Product($item)), $item['quantity'] ?? 0);
                        return $item['quantity'] * $price;
                    }),

            ])->title('Позиции в заказе'),

            Layout::view('components.orchid.table-total-price-component', ['total' => $total,]),

            Layout::rows([

                ViewField::make('order')->view('components.orchid.product-delivery-component'),

                OrderStatusButton::getStatusButton($this->order),

            ])->title('Доставка'),

            Layout::component(OrderStatusComponent::class),

            Layout::columns([

                Layout::legend('order', [
                    Sight::make('name', 'Имя'),
                    Sight::make('surname', 'Фамилия'),
                    Sight::make('father_name', 'Отчество'),
                    Sight::make('user_id', 'ID'),
                    Sight::make('client.email', 'E-mail'),
                    Sight::make('phone', 'Телефон'),
                    Sight::make('client.created_at', 'Дата регистрации')
                        ->render(function (Order $order) {
                            if ($order->created_at)
                                return $order->created_at->format('d.m.Y');
                            return '';
                        }),
                ])->title('Личные данные'),

                Layout::legend('order', [
                    Sight::make('delivery.city', 'Город'),
                    Sight::make('delivery.adress', 'Адрес'),
                    Sight::make('delivery.passport_series', 'Серия паспорта'),
                    Sight::make('delivery.passport_number', 'Номер паспорта'),
                ])->title('Указанный адрес доставки'),

            ]),

        ];
    }

    /**
     * @param $orderId
     * @param $newStatusId
     * @return void
     */
    public function setStatus($orderId, $newStatusId)
    {
        Order::find($orderId)->events()->create([
            'event_id' => $newStatusId,
        ]);
    }
}
