<?php

namespace App\Orchid\Screens\Order;

use App\Models\Order;
use App\Models\OrderEvent;
use App\View\Components\Orchid\OrderStatusButtons\WaitForPaymentComponent;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\ViewField;
use Orchid\Support\Color;

class OrderStatusButton
{
    /**
     * @var string[]
     */
    private static $views = [
        OrderEvent::EVENT_WAITING_FOR_PAYMENT => 'wait-for-payment-component',
    ];

    /**
     * @param \App\Models\Order $order
     * @return \Orchid\Screen\Field
     */
    public static function getStatusButton(Order $order)
    {
        $button = self::getButton($order);

        if (isset(static::$views[$order->eventService()->nextStatus()])) {
            return ViewField::make('order')
                ->view('components.orchid.order-status-buttons.'
                    . static::$views[$order->eventService()->nextStatus()]);
        }
        return $button;
    }

    /**
     * @param \App\Models\Order $order
     * @return \Orchid\Screen\Field
     */
    public static function getButton(Order $order)
    {
        return Button::make(__('admin.order.statuses.buttons.' . $order->payment_type . '.' . $order->eventService()->getStatus()))
            ->type(Color::PRIMARY())
            ->method('setStatus')
            ->parameters([
                'orderId' => $order->id,
                'status' => $order->eventService()->nextStatus(),
            ])
            ->hidden(!($order->eventService()->nextStatus() ?? null));
    }
}
