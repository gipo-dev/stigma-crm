<?php

namespace App\Orchid\Screens\ParentProduct;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductType;
use App\Orchid\Layouts\EditScreenButtons;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ParentProductEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование товара';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Product
     */
    private $product;

    /**
     * Query data.
     *
     * @param \App\Models\Product $product
     * @return array
     */
    public function query(Product $product): array
    {
        $this->product = $product;

        if (!$this->product->exists)
            $this->name = 'Добавление товара';

        return [
            'product' => $product,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons(
            $this->product->id,
//            $this->product->exists ? route('page.products.person', $this->person->slug) : null,
        );
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [

            Layout::columns([
                Layout::rows([
                    Input::make('product.name')->title('Наименование')
                        ->maxlength(255)->required(),
                    Relation::make('product.category_id')->fromModel(ProductCategory::class, 'name')
                        ->title('Категория')->required(),

                    Input::make('product.id')->hidden(),

                    EditScreenButtons::saveButton('Сохранить товар'),
                ]),
            ]),

        ];
    }

    /**
     * @param \App\Models\Product $product
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function save(Product $product, Request $request)
    {
        $product->fill($request->product)->save();
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        ProductType::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.products'));
    }


}
