<?php

namespace App\Orchid\Screens\Client;

use App\Models\User;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ClientListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Клиенты';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'clients' => User::whereHas('roles', function ($q) {
                return $q->where('slug', 'client');
            })->filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить клиента')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.clients.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('clients', [

                TD::make('id', 'ID'),

                TD::make('name', 'Имя')
                    ->render(function (User $user) {
                        return Link::make($user->short_name)->route('platform.clients.edit', $user);
                    }),

                TD::make('city', 'Город'),

                TD::make('phone', 'Телефон'),

                TD::make('email', 'E-mail'),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (User $user) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.clients.edit', $user)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $user->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        User::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
