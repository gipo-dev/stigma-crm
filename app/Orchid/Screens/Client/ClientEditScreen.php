<?php

namespace App\Orchid\Screens\Client;

use App\Http\Requests\ClientRequest;
use App\Models\User;
use App\Orchid\Layouts\EditScreenButtons;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Orchid\Platform\Models\Role;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ClientEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование клиента';

    /**
     * @var \App\Models\User
     */
    private $user;

    /**
     * Query data.
     *
     * @param \App\Models\User $user
     * @return array
     */
    public function query(User $user): array
    {
        $this->user = $user;

        if (!$this->user->exists)
            $this->name = 'Новый клиентский профиль';

        return [
            'user' => $user,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons(
            $this->user->id,
//            $this->product->exists ? route('page.products.person', $this->person->slug) : null,
        );
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        $password = Str::random(10);
        $passwordFields = [];
        if (!$this->user->exists) {
            $this->user->open_password = $password;
            $passwordFields[] = Input::make('user.password')
                ->value(Hash::make($password))->hidden();
        }

        return [

            Layout::columns([

                Layout::rows([
                    Input::make('user.username')->title('Никнейм')->required(),
                    Input::make('user.name')->title('Имя')->required(),
                    Input::make('user.surname')->title('Фамилия'),
                    Input::make('user.father_name')->title('Отчество'),
                    DateTimer::make('user.birthday')->title('Дата рождения'),
                    Input::make('user.phone')->title('Телефон'),
                    Input::make('user.email')->type('email')->title('E-mail'),
                    Input::make('user.city')->title('Город'),

                    EditScreenButtons::saveButton('Сохранить профиль'),
                ])->title('Персональные данные'),

                Layout::rows(array_merge([
//                    Input::make('user.id')->title('ID')->readonly(),
                    Input::make('user.id')->hidden(),
                    Input::make('')->title('Логин для входа')->value($this->user->username)->readonly(),
                    Input::make('user.open_password')->title('Пароль для входа')->readonly(),
                ], $passwordFields))->title('Данные для входа в онлайн-магазин'),

            ]),

        ];
    }


    /**
     * @param \App\Models\ProductType $product
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function save(User $user, ClientRequest $request)
    {
        $user->fill($request->user)->save();
        if ($user->wasRecentlyCreated)
            $user->addRole(Role::where('slug', 'client')->first());
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        User::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.clients'));
    }

}
