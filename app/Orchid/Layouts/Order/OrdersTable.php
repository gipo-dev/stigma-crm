<?php

namespace App\Orchid\Layouts\Order;

use App\Models\Order;
use App\View\Components\Orchid\TableClientComponent;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Support\Color;

class OrdersTable extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'orders';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('name', 'Имя')
                ->component(TableClientComponent::class, 'order'),

            TD::make('id', 'Номер заказа'),

            TD::make('created_at', 'Оформлен')
                ->render(function (Order $order) {
                    if ($order->created_at)
                        return $order->created_at->format('H:i d.m.Y');
                    return '';
                }),

            TD::make('payment_type', 'Оплата')
                ->render(function (Order $order) {
                    return __('admin.order.payment_type.' . $order->payment_type);
                }),

            TD::make('payment_type', 'Статус')
                ->render(function (Order $order) {
                    $status = $order->eventService()->nextStatus();
                    $color = __('admin.order.statuses.colors.' . $status);
                    return '<span class="badge btn-' . $color . '">'
                        . __('admin.order.statuses.titles.' . $status) . '</span>';
                }),

            TD::make('', 'Подробности')->render(function (Order $order) {
                return Link::make('Подробности')
                    ->route('platform.orders.edit', $order)
                    ->type(Color::SUCCESS())
                    ->icon('eye');
            }),
        ];
    }
}
