<?php


namespace App\Orchid\Layouts;


use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Support\Color;

class EditScreenButtons
{
    /**
     * @param int|null $deleteId
     * @param string|null $viewRoute
     * @return array
     */
    public static function defaultButtons(int $deleteId = null, string $viewRoute = null): array
    {
        return [
            Button::make('Удалить')
                ->icon('trash')
                ->confirm('Подтверждаете удаление?')
                ->method('delete')
                ->type(Color::DANGER())
                ->canSee(!is_null($deleteId))
                ->parameters([
                    'id' => $deleteId,
                ]),

            Link::make('Перейти')
                ->icon('arrow-right-circle')
                ->type(Color::PRIMARY())
                ->href($viewRoute ?? '')
                ->target('_blank')
                ->canSee(!is_null($viewRoute)),

            Button::make('Сохранить')
                ->icon('check')
                ->type(Color::SUCCESS())
                ->method('save'),
        ];
    }

    /**
     * @param string $text
     * @param string $method
     * @return \Orchid\Screen\Actions\Button
     */
    public static function saveButton($text = 'Сохранить', $method = 'save')
    {
        return Button::make($text)
            ->icon('check')
            ->type(Color::SUCCESS())
            ->method($method);
    }
}
